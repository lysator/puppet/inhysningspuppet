# Install dependencies for wordpress
class wordpress::install {
  package { [ 'php', 'libapache2-mod-php', 'php-mysql', 'php-gd' ]:
    ensure => latest,
  }

  file { '/srv/wordpress':
    ensure => 'directory',
  } -> exec { 'download wordpress':
    command => 'wget https://wordpress.org/latest.tar.gz',
    cwd     => '/tmp',
    creates => '/tmp/latest.tar.gz',
    path    => ['/bin', '/usr/bin',],
  } ~> exec { 'extract wordpress':
    command => 'tar xzf /tmp/latest.tar.gz',
    creates => '/srv/wordpress/wordpress',
    cwd     => '/srv/wordpress',
    path    => ['/bin', '/usr/bin',],
  }

  file { ['/etc/letsencrypt/archive', '/etc/letsencrypt/live' ]:
    ensure => directory,
    group  => 'www-data',
    mode   => '0750',
  }

  file { '/var/wordpress_dump':
    ensure => directory,
  }
}
