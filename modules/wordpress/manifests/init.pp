# Creates and manages wordpress directories
define wordpress (
  String $domain = 'lysator.liu.se',
  String $url = "${name}.${domain}",
  Array[String] $extra_url = [],
  Optional[Enum['nginx', 'apache']] $webserver = undef,
  String $wp_root = '/srv/wordpress',
  Boolean $debug = false,
) {
  require ::wordpress::install

  include ::mysql::server

  $db_name = "wp-${name}";
  $db_user = "wp-${name}";
  $db_password = 'Hunter2'; # TODO
  $db_host = '127.0.0.1';

  include wordpress::install

  mysql::db { $db_name:
    user     => $db_user,
    password => $db_password,
    host     => $db_host,
    grant    => 'all',
  }

  $wp_path = "${wp_root}/${url}"

  file { $wp_path:
    ensure  => directory,
    source  => "${wp_root}/wordpress",
    recurse => remote,
    owner   => 'www-data',
    group   => 'www-data',
  }
  -> file { "${wp_path}/wp-config.php":
    ensure  => present,
    owner   => 'www-data',
    group   => 'www-data',
    content => epp('wordpress/wp-config.php.epp', {
      'db_name'     => $db_name,
      'db_user'     => $db_user,
      'db_password' => $db_password,
      'db_host'     => $db_host,
      'debug'       => $debug,
    }),
  }

  chmod_r { $wp_path:
    want_mode => '0664',
  }

  file { "/var/wordpress_dump/${url}":
    ensure => directory,
  }

  file { "${wp_path}/mysqldump.sh":
    ensure  => file,
    mode    => '0755',
    content => epp('wordpress/mysql_dump.sh.epp', {
            'path'     => "/var/wordpress_dump/${url}",
            'user'     => $db_user,
            'password' => $db_password,
            'db_name'  => $db_name,
            'db_host'  => $db_host,
            }),
  }

  cron { "mysqldump ${url}":
    ensure  => present,
    command => "${wp_path}/mysqldump.sh",
    user    => root,
    hour    => 3,
    minute  => 35,
    require => [ File["/var/wordpress_dump/${url}"], File["${wp_path}/mysqldump.sh"]],
  }


  case $webserver {
    'apache': {
      include ::apache
      include apache::mod::rewrite
      include apache::mod::php
      ensure_packages (['python-certbot-apache'], { ensure => installed })

      # TODO php version?

      ini_setting { 'php apache2 post_max_size':
        ensure            => present,
        path              => '/etc/php/7.2/apache2/php.ini',
        key_val_separator => '=',
        section           => 'PHP',
        setting           => 'post_max_size',
        value             => '0', # unlimited
      }

      ini_setting { 'php apache2 upload_max_filesize':
        ensure            => present,
        path              => '/etc/php/7.2/apache2/php.ini',
        key_val_separator => '=',
        section           => 'PHP',
        setting           => 'upload_max_filesize',
        value             => '1024M',
      }

      # TODO these should depend on tye
      letsencrypt::certonly { $url:
        domains              => $extra_url << $url,
        manage_cron          => true,
        suppress_cron_output => true,
        cron_hour            => '4',
        cron_minute          => '17',
        plugin               => 'apache',
      }


      ($extra_url << $url).each |$name| {
        apache::vhost { "${name}_non-ssl":
          servername        => $name,
          port              => '80',
          docroot           => $wp_path,
          override          => ['All'],
          redirect_status   => 'permanent',
          redirect_dest     => "https://${name}/",
          before            => Letsencrypt::Certonly[$url],
          access_log_format => '%h %u %t %m %H://%v/%U%q %>s %b',
          access_log_file   => "${url}_access.log",
          error_log_file    => "${url}_error.log",
        }

        apache::vhost { "${name}_ssl":
          servername        => $name,
          port              => '443',
          docroot           => $wp_path,
          override          => ['All'],
          ssl               => true,
          ssl_cert          => "/etc/letsencrypt/live/${url}/fullchain.pem",
          ssl_key           => "/etc/letsencrypt/live/${url}/privkey.pem",
          require           => Letsencrypt::Certonly[$url],
          access_log_format => '%h %u %t %m %H://%v/%U%q %>s %b',
          access_log_file   => "${url}_access.log",
          error_log_file    => "${url}_error.log",
        }
      }
    }
    'nginx': {
      fail('Auto setup for nginx not supported yet')
    }
    default: {
      # NOOP
    }
  }
}

