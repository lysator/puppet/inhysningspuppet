# Users on system.
class liufs::users {

  file_line { 'default shell':
    ensure => present,
    path   => '/etc/default/useradd',
    line   => 'SHELL=/bin/bash',
    match  => '^SHELL=',
  }

  mysql_plugin { 'auth_socket':
    ensure => present,
  }

  # change
  # ensure => present
  # to
  # ensure => absent
  # to remove a user

  user { 'hugo':
    ensure     => 'present',
    comment    => '',
    membership => 'inclusive',
    groups     => [
      'sudo', 'lxd', 'ftp',
    ]
  }

  mysql_user { 'hugo@localhost':
    ensure => present,
    plugin => 'auth_socket'
  }
  mysql_grant { 'hugo@localhost/*.*':
    require    => Mysql_User['hugo@localhost'],
    privileges => [ 'SELECT', ],
    user       => 'hugo@localhost',
    table      => '*.*',
  }

  user { 'green':
    ensure     => 'present',
    membership => 'inclusive',
    groups     => [
      'sudo', 'lxd', 'ftp',
    ]
  }

  user { 'kent':
    ensure     => 'present',
    membership => 'inclusive',
    groups     => [
      'sudo', 'lxd', 'ftp',
    ]
  }

  user { 'fabian':
    ensure     => 'present',
    membership => 'inclusive',
    groups     => [
      'www-data',
    ]
  }

  mysql_user { 'fabian@localhost':
    ensure => present,
    plugin => 'auth_socket'
  }
  mysql_grant { 'fabian@localhost/*.*':
    require    => Mysql_User['fabian@localhost'],
    privileges => [ 'SELECT', ],
    user       => 'fabian@localhost',
    table      => '*.*',
  }
  mysql_grant { 'fabian@localhost/wp-liufs.*':
    require    => Mysql_User['fabian@localhost'],
    privileges => [ 'ALL' ],
    user       => 'fabian@localhost',
    table      => 'wp-liufs.*',
  }
}
