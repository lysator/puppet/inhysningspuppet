node 'holgerspexet' {
  require ::baseinstall
  require ::puppetfetch
  include ::ssh
  include ::insidan
  include ::arkivet
  include ::sjung
  include ::lysbackup
}

node 'holgerspexet-public' {
  require ::baseinstall
  require ::puppetfetch
  include ::ssh
  include ::wordpress
  include ::lysbackup
}

node 'd-group' {
  require ::baseinstall

  class { '::letsencrypt':
    email => 'webb@d-group.se',
  }
}

node 'liufs' {
  require ::baseinstall
  # require ::puppetfetch
  # include ::ssh
  class { 'lysbackup':
    # make sure this user exists on the backup host
    backup_user => 'liufs-test',
  }

  class { '::letsencrypt':
    email => 'webmaster@liuformulastudent.se',
  }

  class { 'apache':
    default_vhost => false,
    mpm_module    => 'prefork',
  }

  $apache_env = {
    'APACHE_PID_FILE'  => '/var/run/apache2/apache2.pid',
    'APACHE_RUN_DIR'   => '/var/run/apache2',
    'APACHE_LOCK_DIR'  => '/var/lock/apache2',
    'APACHE_RUN_USER'  => 'www-data',
    'APACHE_RUN_GROUP' => 'www-data',
  }
  file { '/etc/apache2/envvars':
    content => join(map($apache_env) |$key, $value| { "export ${key}=${value}" }, "\n"),
  }

  # NOTE wordpress plugins aren't handled here, but by wordpress
  # itself. The important directories seems to be
  # wp-content/{plugins,themes}.
  wordpress { 'liufs':
    extra_url => [ 'liuformulastudent.se', ],
    webserver => apache,
  }

  include ::liufs::users
}
