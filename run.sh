#!/bin/bash

FLAGS=""

while [ -n "$1" ]; do
	case "$1" in
		dry)
			FLAGS+="--noop --show_diff"
			shift
			;;
		*)
			break
	esac
done

PATH=$PATH:/opt/puppetlabs/bin

set -x

puppet apply --modulepath=modules manifests/site.pp $FLAGS "$@"
